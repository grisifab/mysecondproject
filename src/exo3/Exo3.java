package exo3;

import java.util.Scanner;

public class Exo3 {

	public static void main(String[] args) {
		// switch case
		
		Scanner monScanner = new Scanner(System.in);
		System.out.println("destination : ?");
		String dest = monScanner.next();
		monScanner.nextLine(); //pour vider le scanner
		monScanner.close();
		
		switch (dest) {
		case "Corse":
			System.out.println("ho Doume !");
			break;
		case "Crete":
			System.out.println("met de l'huile");
			break;
		case "Mikonos":
			System.out.println("salut Philou");
			break;
		case "Ibiza":
			System.out.println("Fiesta");
			break;
		default:
			System.out.println("pas de vacances");
		}
	}

}
