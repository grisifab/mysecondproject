package exo10;

import java.util.Scanner;

public class Exo10 {

	public static void main(String[] args) {
		// �crivez un programme Java qui donne le jour de la semaine. 
		
		String[] JdS = {"Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"};
		
		Scanner monScanner = new Scanner(System.in);
		
		System.out.println("Rentrer un nombre de semaine");
		int num = monScanner.nextInt();
		monScanner.nextLine(); //pour vider le scanner
		monScanner.close();
		
		System.out.println(JdS[(num - 1)]);

		// Autre sol
		
		switch (num) {
		case 1 :
			System.out.println("Lundi");
			break;
		case 2 :
			System.out.println("Mardi");
			break;
		case 3 :
			System.out.println("Mercredi");
			break;
		case 4 :
			System.out.println("Jeudi");
			break;
		case 5 :
			System.out.println("Vendredi");
			break;
		case 6 :
			System.out.println("Samedi");
			break;
		case 7 :
			System.out.println("Dimanche");
			break;
		default :
			System.out.println("sbooby");
		}
	}

}
