package exo7;

import java.util.Scanner;

public class Exo7 {

	public static void main(String[] args) {
		// �crivez un programme Java pour tester un nombre positif ou n�gatif. 
		
		Scanner monScanner = new Scanner(System.in);
		System.out.println("Rentrer un nombre entier");
		
		int tent = monScanner.nextInt();
		monScanner.nextLine(); //pour vider le scanner

		System.out.println((tent < 0) ? "n�gatif" : "positif");
		
		monScanner.close();
		
	}

}
