package exo13;

import java.util.Scanner;

public class Exo13 {

	public static void main(String[] args) {
		// �crivez un programme Java qui trouve le nombre de jours dans un mois.
		// avec un switch case
		
		int res, mois;
		
		Scanner monScanner = new Scanner(System.in);
		
		System.out.println("Rentrer un mois 1=>12");
		mois = monScanner.nextInt();
		monScanner.nextLine(); //pour vider le scanner
		
		switch (mois) {
		case 1 :
			res = 31;
			break;
		case 2 :
			System.out.println("Rentrer une ann�e 20xx");
			int annee = monScanner.nextInt();
			monScanner.nextLine(); //pour vider le scanner
			res = ((annee % 4) == 0) ? 29 : 28;
			break;
		case 3 :
			res = 31;
			break;
		case 4 :
			res = 30;
			break;
		case 5 :
			res = 31;
			break;
		case 6 :
			res = 30;
			break;
		case 7 :
			res = 31;
			break;
		case 8 :
			res = 31;
			break;
		case 9 :
			res = 30;
			break;
		case 10 :
			res = 31;
			break;
		case 11 :
			res = 30;
			break;
		case 12 :
			res = 31;
			break;
		default :
			res = 0;
		}
		monScanner.close();
		System.out.println("Le mois " + mois + " compte " + res + " jours !");
	}

}
