package exo2;

import java.util.Scanner;

public class Exo2 {

	public static void main(String[] args) {
		// l'utilisateur saisie son age, on lui indique si majeur ou mineur

		Scanner monScanner = new Scanner(System.in);
		System.out.println("ton age : ?");
		int num = monScanner.nextInt();
		monScanner.close();
		if (num < 18) {
			System.out.println("Mineur");
		} else {
			System.out.println("Majeur");
		}

	}

}
