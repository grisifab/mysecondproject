package exo6;

import java.util.Scanner;

public class Exo6 {

	public static void main(String[] args) {
		
		Scanner monScanner = new Scanner(System.in);
		int chiffre = (int)(Math.random()*1000);
		int tent;
		System.out.println("Trouver le nombre entier secret [0 1000]");
		
		do {
			System.out.println("votre proposition : ");
			tent = monScanner.nextInt();
			monScanner.nextLine(); //pour vider le scanner
			
			if (tent > chiffre) {
				System.out.println("Trop grand");
			} else if (tent < chiffre) {
				System.out.println("Trop petit");
			}
		} while (tent != chiffre);
		
		System.out.println("Gagn�");
		monScanner.close();
	}
}
