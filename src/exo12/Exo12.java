package exo12;

import java.util.Scanner;

public class Exo12 {

	public static void main(String[] args) {
		// �crivez un programme Java qui trouve le nombre de jours dans un mois.
	
		int[] JdM = {0,31,28,31,30,31,30,31,31,30,31,30,31};
		int res;
		
		Scanner monScanner = new Scanner(System.in);
		
		System.out.println("Rentrer un mois 1=>12");
		int mois = monScanner.nextInt();
		monScanner.nextLine(); //pour vider le scanner
		
		
		if (mois == 2) {
			System.out.println("Rentrer une ann�e 20xx");
			int annee = monScanner.nextInt();
			monScanner.nextLine(); //pour vider le scanner
			res = ((annee % 4) == 0) ? 29 : 28;
			System.out.println("Le mois " + mois + " de l'ann�e " + annee + " compte " + res + " jours !");
		} else {
			res = JdM[mois];
			System.out.println("Le mois " + mois + " compte " + res + " jours !");
		}
	
		monScanner.close();
	
	}

}
