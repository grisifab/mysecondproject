package exo5;

import java.util.Scanner;



public class Exo5 {

	public static void main(String[] args) {
		
		Scanner monScanner = new Scanner(System.in);
		int i = 1;
		int chiffre = 512;
		int tent = -1000;
		
		System.out.println("Trouver le nombre entier secret [0 1000] en 10 tentatives");
		
		while ((tent != chiffre) && (i<=10))
		{
			System.out.println("Tentative N � " + i + " : ");
			tent = monScanner.nextInt();
			monScanner.nextLine(); //pour vider le scanner
			i ++;
			if (tent > chiffre) {
				System.out.println("Trop grand");
			} else if (tent < chiffre) {
				System.out.println("Trop petit");
			}
		}
		
		monScanner.close();
		
		if (tent == chiffre) {
			System.out.println("Gagn�");
		} else {
			System.out.println("Perdu");
		}

	}

}
