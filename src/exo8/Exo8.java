package exo8;

import java.util.Scanner;

public class Exo8 {

	public static void main(String[] args) {
		// Demandez trois nombres de l'utilisateur et affichez le plus grand nombre
		Scanner monScanner = new Scanner(System.in);
		
		int num = -2147483648; 
		
		for (int i = 0; i < 3; i++) {
			System.out.println("Rentrer un nombre entier");
			num = Math.max(monScanner.nextInt(),num);
			monScanner.nextLine(); //pour vider le scanner
		}
		monScanner.close();
		System.out.println("Le plus grand nombre renseigné est : " + num);
	}

}
