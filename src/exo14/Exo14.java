package exo14;

import java.util.Scanner;

public class Exo14 {

	public static void main(String[] args) {
		// �crire un programme Java pour convertir des minutes en un certain nombre d'ann�es et de jours
		int minute, heure, jour, annee;
		
		Scanner monScanner = new Scanner(System.in);
		System.out.println("Rentrer le nombre de minutes");
		minute = monScanner.nextInt();
		monScanner.nextLine(); //pour vider le scanner
		monScanner.close();
		
		annee = minute / 525600;
		minute -= annee * 525600;
		jour = minute / 1440;
		minute -= jour * 1440;
		heure = minute / 60;
		minute -= heure *60;
		
		System.out.println("soit " + minute + " minutes, " + heure + " heures, " + jour + " jours et " + annee + " ann�es.");
		
	}

}
