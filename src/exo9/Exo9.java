package exo9;

import java.util.Scanner;

public class Exo9 {

	public static void main(String[] args) {
		// �crivez un programme Java qui lit un nombre � virgule 
		//et affiche "z�ro" si le nombre est z�ro. 
		//Sinon, affiche "positif" ou "n�gatif". 
		//Puis, affiche "petit" si la valeur absolue du nombre est inf�rieur � 1 
		//ou "grand" s'il d�passe 1 000 000
		
		Scanner monScanner = new Scanner(System.in);
		
		System.out.println("Rentrer un nombre r�el");
		float num = monScanner.nextFloat();
		monScanner.nextLine(); //pour vider le scanner
		monScanner.close();
		
		if(num == 0) {
			System.out.println("Z�ro");
		} else if (num > 0) {
			System.out.println("Positif");
		} else {
			System.out.println("N�gatif");
		}
		
		float absnum = Math.abs(num);
		
		if (absnum < 1) {
			System.out.println("Petit");
		}
		
		if (num > 1000000) {
			System.out.println("Grand");
		}
		
	}

}
